#!/usr/bin/env bash

PROJECT="drupal8-composer"
WEBROOT="/var/app/drupal8-composer/web"

cd /var/app/
composer create-project drupal-composer/drupal-project:8.x-dev $PROJECT --stability dev --no-interaction

cd /var/app/$PROJECT/
composer install
./vendor/bin/drush site-install standard -y --db-url='mysql://root:dog3%9out@mysql/drupal8_web' \
  --debug --root=$WEBROOT

composer require drupal/address
composer require drupal/admin_toolbar
composer require drupal/bootstrap
composer require drupal/coffee
composer require drupal/default_content
composer require drupal/devel
composer require drupal/diff
composer require drupal/ds
composer require drupal/entity_clone
composer require drupal/features
composer require drupal/field_group
composer require drupal/google_analytics
composer require drupal/linkit
composer require drupal/masquerade
composer require drupal/metatag
composer require drupal/paragraphs
composer require drupal/paragraphs_admin
composer require drupal/pathauto
composer require drupal/rabbit_hole
composer require drupal/simplify
composer require drupal/token
composer require drupal/view_mode_page
composer require drupal/webform

./vendor/bin/drush --root=$WEBROOT pm-enable -y admin_toolbar coffee ds features field_group paragraphs \
  paragraphs_admin pathauto rabbit_hole simplify token webform
