PROJECT_CONTAINER='docker exec -it web'
DOCROOT='/var/app/drupal8-composer/web'

# Drupal console commands
function docker-drupal () {
    time ${PROJECT_CONTAINER} bin/drupal --root=$DOCROOT "$@"
}
# Composer commands
function docker-composer () {
  export COMPOSER_PROCESS_TIMEOUT=600
  time  ${PROJECT_CONTAINER} composer "$@" -vvv --profile
  # time  ${PROJECT_CONTAINER} bash -c `export COMPOSER_PROCESS_TIMEOUT=600 && composer "$@" -vvv --profile`
  # time  ${PROJECT_CONTAINER} bash -c `export COMPOSER_PROCESS_TIMEOUT=600 && composer "$@" -vvv --profile`
}
# Drush commands
function docker-drush () {
  time ${PROJECT_CONTAINER} vendor/bin/drush --root=$DOCROOT "$@"
}
# Allow custom commands against the container
function docker-run () {
  time ${PROJECT_CONTAINER} "$@"
}
# Allow custom commands against the container
function crs () {
  time composer run-script "$@"
}
