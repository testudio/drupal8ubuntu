# Drupal machine

Create VM instance and run Drupal on your machine.

1. Launch instance on preferred cloud provider, e.g. AWS, DigitalOcean or GCloud. Usually 2Gb with 1 processor is enough.

    1.1 For next steps you can run `bash bin/setup-machine.sh` or follow step 2 to 5.
    Make sure you have `git` installed.
    
    ```
      sudo mkdir /app
      sudo chown $(whoami) /app/
      cd /app
      git clone https://gitlab.com/testudio/drupal8ubuntu.git
      cd drupal8ubuntu/
      bash bin/setup-machine.sh
    ```

2. Install [docker ce](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/) and [docker compose](https://docs.docker.com/compose/install/`).
  
    ```
      sudo apt-get update
      sudo apt-get upgrade
      
      sudo apt-get install \
          apt-transport-https \
          ca-certificates \
          curl \
          software-properties-common
          
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
      
      sudo add-apt-repository \
         "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
         $(lsb_release -cs) \
         stable"
         
      sudo apt-get update
      
      sudo apt-get install docker-ce
      
    ```
  
3. Install [docker compose](https://docs.docker.com/compose/install/#install-compose).
  
    ```
      sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
      
      sudo chmod +x /usr/local/bin/docker-compose
     
    ```  
4. Run [post install commands](https://docs.docker.com/engine/installation/linux/linux-postinstall/#manage-docker-as-a-non-root-user).
  
    ```
      sudo groupadd docker
     
      sudo usermod -aG docker $USER
    ```
    
    Exit and reconnect to the machine.
 
 5. Build docker containers for Drupal, mysql and phpmyadmin.
      
    ```
      cd /app/drupal8ubuntu/
      git pull
      docker-compose up -d --build
    ```
     
 6. Install Drupal
      
    ```
      . bin/dot.bashrc 
      docker-run bash bin/setup-drupal.sh
    ```
    
    